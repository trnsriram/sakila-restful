package sakila.service;

import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { sakila.domain.Actor.class })
public interface ActorService {
}
