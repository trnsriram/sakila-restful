// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sakila.service;

import java.util.List;
import sakila.domain.FilmActor;
import sakila.domain.FilmActorPK;
import sakila.service.FilmActorService;

privileged aspect FilmActorService_Roo_Service {
    
    public abstract long FilmActorService.countAllFilmActors();    
    public abstract void FilmActorService.deleteFilmActor(FilmActor filmActor);    
    public abstract FilmActor FilmActorService.findFilmActor(FilmActorPK id);    
    public abstract List<FilmActor> FilmActorService.findAllFilmActors();    
    public abstract List<FilmActor> FilmActorService.findFilmActorEntries(int firstResult, int maxResults);    
    public abstract void FilmActorService.saveFilmActor(FilmActor filmActor);    
    public abstract FilmActor FilmActorService.updateFilmActor(FilmActor filmActor);    
}
