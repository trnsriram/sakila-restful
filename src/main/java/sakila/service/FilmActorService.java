package sakila.service;

import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { sakila.domain.FilmActor.class })
public interface FilmActorService {
}
