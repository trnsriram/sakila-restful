package sakila.domain;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.entity.RooJpaEntity;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaEntity(identifierType = FilmActorPK.class, versionField = "", table = "film_actor")
@RooDbManaged(automaticallyDelete = true)
@RooJson(deepSerialize = true)
public class FilmActor {
}
