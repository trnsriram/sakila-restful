// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sakila.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import sakila.domain.Staff;

privileged aspect Staff_Roo_Jpa_Entity {
    
    declare @type: Staff: @Entity;
    
    declare @type: Staff: @Table(name = "staff");
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "staff_id")
    private Short Staff.staffId;
    
    public Short Staff.getStaffId() {
        return this.staffId;
    }
    
    public void Staff.setStaffId(Short id) {
        this.staffId = id;
    }
    
}
