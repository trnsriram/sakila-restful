package sakila.web;

import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sakila.domain.Store;

@RooWebJson(jsonObject = Store.class)
@Controller
@RequestMapping("/stores")
public class StoreController {
}
