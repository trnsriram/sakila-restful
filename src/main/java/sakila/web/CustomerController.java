package sakila.web;

import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sakila.domain.Customer;

@RooWebJson(jsonObject = Customer.class)
@Controller
@RequestMapping("/customers")
public class CustomerController {
}
