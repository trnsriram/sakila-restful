package sakila.web;

import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sakila.domain.Inventory;

@RooWebJson(jsonObject = Inventory.class)
@Controller
@RequestMapping("/inventorys")
public class InventoryController {
}
