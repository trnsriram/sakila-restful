package sakila.web;

import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sakila.domain.Staff;

@RooWebJson(jsonObject = Staff.class)
@Controller
@RequestMapping("/staffs")
public class StaffController {
}
