package sakila.repository;

import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;
import sakila.domain.Actor;

@RooJpaRepository(domainType = Actor.class)
public interface ActorRepository {
}
