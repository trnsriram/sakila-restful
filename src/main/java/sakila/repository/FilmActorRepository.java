package sakila.repository;

import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;
import sakila.domain.FilmActor;

@RooJpaRepository(domainType = FilmActor.class)
public interface FilmActorRepository {
}
