// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package sakila.domain;

import org.springframework.beans.factory.annotation.Configurable;
import sakila.domain.StoreDataOnDemand;

privileged aspect StoreDataOnDemand_Roo_Configurable {
    
    declare @type: StoreDataOnDemand: @Configurable;
    
}
